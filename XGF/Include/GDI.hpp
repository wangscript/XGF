#pragma once
#include "Defines.hpp"
#include <Windows.h>
#include <d3d11_1.h>
#pragma comment(lib,"d3d11.lib")
#pragma comment(lib,"d3dcompiler.lib")
#include <d3dcompiler.h>
#include <vector> 
/*
底层图形接口
调整窗口尺寸请使用ResizeTarget函数，而不是Win API
使用全屏模式先选择一个DisplayMode(DXGI_MODE)：call SetFullScreenDisplayMode（pos）；再call SetFullScreen（true）
目前全屏模式有BUG，请暂时不要使用
*/
class GDI
{
public:
	explicit GDI() {};
	~GDI() {};
	//框架调用
	void Create();
	//框架调用
	void Destory();
	void Clear(float color[]);
	void Clear(Color & c); 
	void ClearDepthStencilBuffer();
	void Present(bool isVsync);
	//初始化函数，必须是该类第一个调用的函数
	//用户调用
	void Initialize(HINSTANCE instance, HWND WndHwnd, HWND TopHwnd, UINT ClientWidth, UINT ClientHeight);
	//框架调用
	void SizeChanged(UINT ClientWidth, UINT ClientHeight);

	void ResizeTarget(UINT x, UINT y);

	void SetFullScreen(bool isFullscreen);

	ID3D11Device * GetDevice() { return mD3dDevice; }
	ID3D11DeviceContext * GetDeviceContext() { return mDeviceContext; }

	void SetFillMode(bool isSold);
	void SetZBufferMode(bool isOpenZBuffer);
	int GetWidth() { return mWidth; }
	int GetHeight() { return mHeight; }
	HWND GetHwnd();
	HWND GetTopHwnd();
	HINSTANCE GetInstance();
	ID3D11DepthStencilView * GetDepthStencilView() { return mDepthStencilView; }
	//恢复RenderTargetView
	void SetRenderTargetView();

	//参数：pos 第几个显示模式
	void SetFullScreenDisplayMode(int pos);
	//获取displaymode list
	//返回值：mode数目
	int GetFullScreenDisplayModes(DXGI_MODE_DESC ** c) { *c = mDisplayMode[0].second; return mDisplayMode[0].first; }
	void OpenDefaultBlendState();
	void CloseBlendState();
	
	void SetDefaultSamplerState();
	void CloseSamplerState();
protected:
	ID3D11Device        *mD3dDevice;
	ID3D11DeviceContext *mDeviceContext;
	IDXGISwapChain * mSwapChain;
	ID3D11RenderTargetView * mRenderTargetView;
	
	ID3D11DepthStencilView * mDepthStencilView;
	ID3D11Texture2D * mDepthStencilBuffer;

	ID3D11BlendState * mDisableBlendState;
	ID3D11BlendState * mBlendState;
	ID3D11SamplerState * mLineSamplerState;
	//开启 Z 深度 缓冲state
	ID3D11DepthStencilState * mDepthStencilState;
	//禁止 Z 深度 缓冲state
	ID3D11DepthStencilState * md3dDisableDepthStencilState;
	//普通模式渲染的RasterState
	ID3D11RasterizerState * mRasterState;
	//线框模式渲染的RasterState
	ID3D11RasterizerState * mFrameRasterState;
	bool mIsOpenFillSold = true;
	bool mIsOpenZBuffer = true;
	//显示的窗口句柄
	HWND mHwnd;
	//顶级窗口句柄
	HWND mTopHwnd;

	UINT mWidth;
	UINT mHeight;
	HINSTANCE mInstance;


	std::vector<std::pair<int, DXGI_MODE_DESC *>> mDisplayMode;
	int mNowInDisplayMode;
	std::vector<IDXGIOutput *> mOutputs;
	std::vector<IDXGIAdapter *> mAdapters;
	void SaveDisplayMode(int c, IDXGIOutput * pDXGIOutput);

	//DXGI_ADAPTER_DESC * mAdapters;
private:
#ifdef _DEBUG
	void QueryInterface();
#endif
private:
	DISALLOW_COPY_AND_ASSIGN(GDI);
};
#define PutDebugString(de) de->SetPrivateData(WKPDID_D3DDebugObjectName, sizeof(_FUNNAME_XGF_), _FUNNAME_XGF_);