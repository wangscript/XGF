#pragma once
#include <DirectXMath.h>

#define COLOR_RED DirectX::XMFLOAT4(1.0f,0.0f,0.0f,1.0f)
#define COLOR_GREEN DirectX::XMFLOAT4(0.0f,1.0f,0.0f,1.0f)
#define COLOR_BLUE DirectX::XMFLOAT4(0.0f,0.0f,1.0f,1.0f)
#define COLOR_BLACK DirectX::XMFLOAT4(0.0f,0.0f,0.0f,1.0f)
#define COLOR_WHITE DirectX::XMFLOAT4(1.0f,1.0f,1.0f,1.0f)



