#           XGF
## What's it?
### A game render framework(XGF) based on direct3d11 API for study game devolopment
##### notic: only used by personal
  
## What Funtions Does It Have？

* A great batch renderer
    *  Texture combine
* Complete animation system
    * Texture animaion
    * Action animation
* DirectInput support
    * Different mouse mode
* UI system
    * Lable
    * EditText
    * Button
* Text renderer based on Freetype
* Basic shape render
    * Line
    * Rectange
    * Circle
* Scene manager
    * Switch scene
    * Scene Aniamtion(Uncompletely)
  
## How To Compile
Compiler： VC++14 or later
If you have Visual Studio or later, you can just open the Soltion(.sln) file. Selecting compiling mode, then compiling with x64 mode.
  
###### notic: Some Demo used resorce, Please Copy these files to run directory. The resorce files always in Bin directory.
 
## Some Thumbnail Preview
 Demo1:  
![DemoMore](https://raw.githubusercontent.com/kadds/XGF/master/TT.PNG)
 Demo2:  
![EasyDemo](https://raw.githubusercontent.com/kadds/XGF/master/TT2.PNG)
 
## Problem Feedback
I'm welcome to feedback. You can use the following contact to communicate with me  
  
* Email: <itmyxyf@gmail.com>
* QQ: 1346144952
  
## Thankes
* [Freetype](https://sourceforge.net/projects/freetype/) 
* [DirectXTK](https://github.com/Microsoft/DirectXT)
* [DirectXTex](https://github.com/Microsoft/DirectXTex)
